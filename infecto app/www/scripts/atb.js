﻿var clcr_value;

//DOSIS PTZ
var Dptz_1 = "4,5gr c/6-8hs";
var Dptz_2 = "2,25g c/6hs";//50 a 20
var Dptz_3 = "2,25gr c/8hs";//20 a dial
var Dptz_dial = "2,25gr c/8hs + 0,75gr post dialisis";

//DOSIS IMI
var Dimi_1 = "250-500mg c/6-8hs";
var Dimi_2 = "250mg c/8-12hs";
var Dimi_3 = "125-250mg c/12hs";
var Dimi_dial = "125-250mg c/12hs post dialisis";

//DOSIS MERO
var Dmero_1 = "1gr c/8hs";
var Dmero_2 = "1gr c/12hs";//50 a 25
var Dmero_3 = "0,5gr c/12hs";//25 a 10
var Dmero_4 = "0,5gr c/24hs";//<10
var Dmero_dial = "0,5gr c/24hs post dialisis";

//DOSIS VANC
var Dvanc_1 = "15-30mg/kg c/12hs";
var Dvanc_2 = "15-30mg/kg c/24-36hs";//atencion
var Dvanc_3 = "7.5mg/kg c/2-3 dias";
var Dvanc_dial = "DOSIS4";

//DOSIS CIPROEV
var Dciproev_1 = "400mg c/12hs";
var Dciproev_2 = "400mg c/24 hs";
var Dciproev_3 = "400mg c/24 hs";
var Dciproev_dial = "400mg c/24hs post dialisis";

//DOSIS CIPROVO
var Dciprovo_1 = "500-750mg c/12hs";
var Dciprovo_2 = "250-500mg c/12hs";
var Dciprovo_3 = "500mg c/24hs";
var Dciprovo_dial = "500mg c/24hs post dialisis";

//DOSIS FLUCO
var Dfluco_1 = "DOSIS1";
var Dfluco_2 = "DOSIS2";
var Dfluco_3 = "DOSIS3";
var Dfluco_dial = "DOSIS4";

//DOSIS ANFO
var Danfo_1 = "DOSIS1";
var Danfo_2 = "DOSIS2";
var Danfo_3 = "DOSIS3";
var Danfo_dial = "DOSIS4";

//DOSIS AMIK
var Damik_1 = "7,5mg/kg c/12hs";
var Damik_2 = "7,5mg/kg c/24hs";
var Damik_3 = "7,5mg/kg c/48hs";
var Damik_dial = "7,5mg/kg c/48hs + 3,25mg/kg post dialisis";

//DOSIS AMPI
var Dampi_1 = "1-2gr c/4-6hs";//>50
var Dampi_2 = "1-2gr c/6-8hs";//50-30
var Dampi_3 = "1-2gr c/8-12hs";//30-10
var Dampi_4 = "1-2gr c/12hs";//<10
var Dampi_dial = "1-2gr c/12hs (dosis previa a dialisis)";//dialisis

//AMPISUL
var Dampisul_1 = "3-1,5gr c/6hs";
var Dampisul_2 = "3-1,5gr c/8-12hs";
var Dampisul_3 = "3-1,5gr c/24hs";
var Dampisul_dial = "3-1,5gr c/24hs (dosis previa a dialisis)";

//CEFAZO
var Dcefazo_1 = "1-2gr c/8hs";
var Dcefazo_2 = "1-2gr c/12hs";
var Dcefazo_3 = "1-2gr c/24-48hs";
var Dcefazo_dial = "1-2gr c/24-48hs + 0,5-1gr post dialisis";

//CEFEPI
var Dcefepi_1 = "2gr c/8-12hs";//>60
var Dcefepi_2 = "2gr c/12hs";//60-30
var Dcefepi_3 = "2gr c/24hs";//29-11
var Dcefepi_4 = "1gr c/24hs";//<10
var Dcefepi_dial = "1gr c/24hs + 1gr post dialisis";//dialisis

//CEFOTA
var Dcefota_1 = "2gr c/8-12hs";
var Dcefota_2 = "2gr c/12-24hs";
var Dcefota_3 = "2gr c/24hs";
var Dcefota_dial = "2gr c/24hs + 1gr post dialisis";

//CEFTAZ
var Dceftaz_1 = "2gr c/8-12hs";
var Dceftaz_2 = "2gr c/12-24hs";
var Dceftaz_3 = "2gr c/24hs";
var Dceftaz_dial = "2gr c/24hs + 1gr post dialisis";

//CEFTRIAXONA
var Dceftri_1 = "1-2gr c/12-24hs";

//CLARITROMICINA
var Dclaritro_1 = "500mg c/12hs";
var Dclaritro_2 = "500mg c/12-24hs";
var Dclaritro_3 = "500mg c/24hs";
var Dclaritro_dial = "500mg c/24hs post dialisis";

//COLISTIN
var Dcolistin_1 = "carga de 300mg + 150mg c/12hs";
var Dcolistin_2 = "carga de 300mg + 150mg c/24hs";
var Dcolistin_3 = "carga de 300mg + 150mg c/48hs";
var Dcolistin_dial = "carga de 300mg + 150mg c/48hs post dialisis";

//DAPTOMICINA
var Ddapto_1 = "4-6mg/kg c/24hs";//>30
var Ddapto_2 = "6mg/kg c/48hs"; //<30
var Ddapto_dial = "6mg/kg c/48hs durante o post dialisis";

//ERTAPENEM
var Derta_1 = "1gr c/24hs";
var Derta_2 = "0,5gr c/24hs";//<30
var Derta_dial = "0,5gr c/24hs + 150mg post dialisis (si se administro 6hs antes de la HD)";

//GENTAMICINA
var Dgenta_1 = "1,7-2mg/kg c/8hs";
var Dgenta_2 = "1,7-2mg/kg c/12-24hs";
var Dgenta_3 = "1,7-2mg/kg c/48hs";
var Dgenta_dial = "1,7-2mg/kg c/48hs + 0,85-1mg/kg post dialisis";

//LINEZOLID
var Dline_1 = "600mg c/12hs";
var Dline_dial = "600mg c/12hs post dialisis";

//TRIMETOPRIMA-SULFAMETOXAZOL
var Dtmssmx_1 = "5-20mg/kg/dia (dividido c/6-12hs)";
var Dtmssmx_2 = "5-10mg/kg/dia (dividido c/12hs)";
var Dtmssmx_3 = "-No se recomienda- 5-10mg/kg/24hs";
var Dtmssmx_dial = "-No se recomienda- 5-10mg/kg/24hs post dialisis";





function onload() {

    var load_clcr1 = localStorage.getItem("stored_clcr");
    var load_clcr2 = JSON.parse(load_clcr1);
    clcr_value = load_clcr2;
    console.log(clcr_value + " loaded");

    var cr_title = document.getElementById("creatinina_atb_page");

    cr_title.innerText = ": " + clcr_value;


    //PTZ
    if (clcr_value >= 50) {
        var dosis = document.getElementById("ptz_dosis");

        dosis.innerText = Dptz_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("ptz_dosis");

        dosis.innerText = Dptz_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("ptz_dosis");

        dosis.innerText = Dptz_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("ptz_dosis");

        dosis.innerText = Dptz_dial;
    }

    //IMI
    if (clcr_value >= 50) {
        var dosis = document.getElementById("imi_dosis");

        dosis.innerText = Dimi_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("imi_dosis");

        dosis.innerText = Dimi_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("imi_dosis");

        dosis.innerText = Dimi_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("imi_dosis");

        dosis.innerText = Dimi_dial;
    }

    //MERO
    if (clcr_value >= 50) {
        var dosis = document.getElementById("mero_dosis");

        dosis.innerText = Dmero_1;

    } else if (clcr_value < 50 && clcr_value >= 25) {
        var dosis = document.getElementById("mero_dosis");

        dosis.innerText = Dmero_2;

    } else if (clcr_value < 25 && clcr_value >= 10) {
        var dosis = document.getElementById("mero_dosis");

        dosis.innerText = Dmero_3;

    } else if (clcr_value < 10) {
        var dosis = document.getElementById("mero_dosis");

        dosis.innerText = Dmero_4;

    }  else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("mero_dosis");

        dosis.innerText = Dmero_dial;
    }

    //VANCO
    if (clcr_value >= 50) {
        var dosis = document.getElementById("vanc_dosis");

        dosis.innerText = Dvanc_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("vanc_dosis");

        dosis.innerText = Dvanc_2;

    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("vanc_dosis");

        dosis.innerText = Dvanc_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("vanc_dosis");

        dosis.innerText = Dvanc_dial;
    }

    //CIPROEV
    if (clcr_value >= 50) {
        var dosis = document.getElementById("ciproev_dosis");

        dosis.innerText = Dciproev_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("ciproev_dosis");

        dosis.innerText = Dciproev_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("ciproev_dosis");

        dosis.innerText = Dciproev_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("ciproev_dosis");

        dosis.innerText = Dciproev_dial;
    }

    //CIPROVO
    if (clcr_value >= 50) {
        var dosis = document.getElementById("ciprovo_dosis");

        dosis.innerText = Dciprovo_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("ciprovo_dosis");

        dosis.innerText = Dciprovo_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("ciprovo_dosis");

        dosis.innerText = Dciprovo_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("ciprovo_dosis");

        dosis.innerText = Dciprovo_dial;
    }

    //FLUCO
    if (clcr_value >= 50) {
        var dosis = document.getElementById("fluco_dosis");

        dosis.innerText = Dfluco_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("fluco_dosis");

        dosis.innerText = Dfluco_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("fluco_dosis");

        dosis.innerText = Dfluco_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("fluco_dosis");

        dosis.innerText = Dfluco_dial;
    }

    //ANFO
    if (clcr_value >= 50) {
        var dosis = document.getElementById("anfo_dosis");

        dosis.innerText = Danfo_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("anfo_dosis");

        dosis.innerText = Danfo_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("anfo_dosis");

        dosis.innerText = Danfo_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("anfo_dosis");

        dosis.innerText = Danfo_dial;
    }

    //AMIKACINA
    if (clcr_value >= 50) {
        var dosis = document.getElementById("amik_dosis");

        dosis.innerText = Damik_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("amik_dosis");

        dosis.innerText = Damik_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("amik_dosis");

        dosis.innerText = Damik_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("amik_dosis");

        dosis.innerText = Damik_dial;
    }

    //AMPICILINA
    if (clcr_value >= 50) {
        var dosis = document.getElementById("ampi_dosis");

        dosis.innerText = Dampi_1;

    } else if (clcr_value < 50 && clcr_value >= 30) {
        var dosis = document.getElementById("ampi_dosis");

        dosis.innerText = Dampi_2;

    } else if (clcr_value < 30 && clcr_value > 10) {
        var dosis = document.getElementById("ampi_dosis");

        dosis.innerText = Dampi_3;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("ampi_dosis");

        dosis.innerText = Dampi_4;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("ampi_dosis");

        dosis.innerText = Dampi_dial;
    }

    //AMPICILINA SULBACTAM
    if (clcr_value >= 50) {
        var dosis = document.getElementById("ampisul_dosis");

        dosis.innerText = Dampisul_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("ampisul_dosis");

        dosis.innerText = Dampisul_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("ampisul_dosis");

        dosis.innerText = Damik_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("ampisul_dosis");

        dosis.innerText = Dampisul_dial;
    }

    //CEFAZOLINA
    if (clcr_value >= 50) {
        var dosis = document.getElementById("cefazo_dosis");

        dosis.innerText = Dcefazo_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("cefazo_dosis");

        dosis.innerText = Dcefazo_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("cefazo_dosis");

        dosis.innerText = Dcefazo_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("cefazo_dosis");

        dosis.innerText = Dcefazo_dial;
    }

    //CEFEPIME
    if (clcr_value >= 60) {
        var dosis = document.getElementById("cefepi_dosis");

        dosis.innerText = Dcefepi_1;

    } else if (clcr_value < 60 && clcr_value >= 30) {
        var dosis = document.getElementById("cefepi_dosis");

        dosis.innerText = Dcefepi_2;
    } else if (clcr_value < 30 && clcr_value > 10) {
        var dosis = document.getElementById("cefepi_dosis");

        dosis.innerText = Dcefepi_3;

    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("cefepi_dosis");

        dosis.innerText = Dcefepi_4;

    }  else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("cefepi_dosis");

        dosis.innerText = Dcefepi_dial;
    }

    //CEFOTAXIMA
    if (clcr_value >= 50) {
        var dosis = document.getElementById("cefota_dosis");

        dosis.innerText = Dcefota_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("cefota_dosis");

        dosis.innerText = Dcefota_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("cefota_dosis");

        dosis.innerText = Dcefota_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("cefota_dosis");

        dosis.innerText = Dcefota_dial;
    }

    //CEFTAZIDIMA
    if (clcr_value >= 50) {
        var dosis = document.getElementById("ceftaz_dosis");

        dosis.innerText = Dceftaz_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("ceftaz_dosis");

        dosis.innerText = Dceftaz_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("ceftaz_dosis");

        dosis.innerText = Dceftaz_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("ceftaz_dosis");

        dosis.innerText = Dceftaz_dial;
    }

    //CEFTRIAXONA
    if (clcr_value > 0) {

        var dosis = document.getElementById("ceftri_dosis");
        dosis.innerText = Dceftri_1;

    }

    //CLARITROMICINA
    if (clcr_value >= 50) {
        var dosis = document.getElementById("claritro_dosis");

        dosis.innerText = Dclaritro_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("claritro_dosis");

        dosis.innerText = Dclaritro_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("claritro_dosis");

        dosis.innerText = Dclaritro_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("claritro_dosis");

        dosis.innerText = Dclaritro_dial;
    }

    //COLISTIN
    if (clcr_value >= 50) {
        var dosis = document.getElementById("colistin_dosis");

        dosis.innerText = Dcolistin_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("colistin_dosis");

        dosis.innerText = Dcolistin_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("colistin_dosis");

        dosis.innerText = Dcolistin_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("colistin_dosis");

        dosis.innerText = Dcolistin_dial;
    }

    //DAPTOMICINA
    if (clcr_value >= 30) {
        var dosis = document.getElementById("dapto_dosis");

        dosis.innerText = Ddapto_1;

    } else if (clcr_value < 30) {
        var dosis = document.getElementById("dapto_dosis");

        dosis.innerText = Ddapto_2;

    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("dapto_dosis");

        dosis.innerText = Ddapto_dial;
    }

    //ERTAPENEM
    if (clcr_value >= 30) {
        var dosis = document.getElementById("erta_dosis");

        dosis.innerText = Derta_1;

    } else if (clcr_value < 30) {
        var dosis = document.getElementById("erta_dosis");

        dosis.innerText = Derta_2;

    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("erta_dosis");

        dosis.innerText = Derta_dial;
    }

    //GENTAMICINA
    if (clcr_value >= 50) {
        var dosis = document.getElementById("genta_dosis");

        dosis.innerText = Dgenta_1;

    } else if (clcr_value < 50 && clcr_value > 10) {
        var dosis = document.getElementById("genta_dosis");

        dosis.innerText = Dgenta_2;
    } else if (clcr_value <= 10) {
        var dosis = document.getElementById("genta_dosis");

        dosis.innerText = Dgenta_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("genta_dosis");

        dosis.innerText = Dgenta_dial;
    }

    //GENTAMICINA
    if (clcr_value === "dialisis") {
        var dosis = document.getElementById("line_dosis");

        dosis.innerText = Dline_dial;

    } else {
        var dosis = document.getElementById("line_dosis");

        dosis.innerText = Dline_1;

    }

    //TRIMETOPRIMA-SULFAMETOXAZOL
    if (clcr_value >= 50) {
        var dosis = document.getElementById("tmssmx_dosis");

        dosis.innerText = Dtmssmx_1;

    } else if (clcr_value < 30 && clcr_value >= 10) {
        var dosis = document.getElementById("tmssmx_dosis");

        dosis.innerText = Dtmssmx_2;
    } else if (clcr_value < 10) {
        var dosis = document.getElementById("tmssmx_dosis");
        $("#tmssmx_dosis").css("color", "red");
        dosis.innerText = Dtmssmx_3;
    } else if (clcr_value === "dialisis") {
        var dosis = document.getElementById("tmssmx_dosis");
        $("#tmssmx_dosis").css("color", "red");
        dosis.innerText = Dtmssmx_dial;
    }


}




document.getElementById("volverhome_but").addEventListener("click", function () {

    window.location.href = "index.html";

});