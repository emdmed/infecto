﻿var clcr_value;

document.getElementById("clcr_but1").addEventListener("click", function () {

    var value1 = document.getElementById("clcr_input").value
    clcr_value = value1
    console.log(clcr_value);

    //store clcr_value

    var save_clcr_value1 = clcr_value;
    var save_clcr_value2 = JSON.stringify(save_clcr_value1);
    localStorage.setItem("stored_clcr", save_clcr_value2);
    console.log("saved");

    window.location.href = "atb.html";


});

document.getElementById("clcr_dialisis").addEventListener("click", function () {

    clcr_value = "dialisis";

    var save_clcr_value1 = clcr_value;
    var save_clcr_value2 = JSON.stringify(save_clcr_value1);
    localStorage.setItem("stored_clcr", save_clcr_value2);
    console.log("saved");

    window.location.href = "atb.html";

});

/*document.getElementById("cd4_menu").addEventListener("click", function () {

    window.location.href = "cd4.html";

});*/


//cockroft gault hombre
document.getElementById("cock_sex_h").addEventListener("click", function () {

    var edad = document.getElementById("cock_edad").value;
    var peso = document.getElementById("cock_peso").value;
    var cr = document.getElementById("cock_cr").value;

    var uno = 140 - +edad;
 
    var dos = +uno * +peso;
   
    var tres = 72 * +cr;
  
    var precuatro = +dos / +tres;
    var cuatro = precuatro.toFixed(0);
    console.log(cuatro + " resultado");

    clcr_value = cuatro;

    var save_clcr_value1 = clcr_value;
    var save_clcr_value2 = JSON.stringify(save_clcr_value1);
    localStorage.setItem("stored_clcr", save_clcr_value2);
    console.log("saved");

    window.location.href = "atb.html";

});

//cockroft gault mujer
document.getElementById("cock_sex_m").addEventListener("click", function () {

    var edad = document.getElementById("cock_edad").value;
    var peso = document.getElementById("cock_peso").value;
    var cr = document.getElementById("cock_cr").value;

    var uno = 140 - +edad;

    var dos = +uno * +peso;

    var tres = 72 * +cr;

    var cuatro = +dos / +tres;

    var precinco = +cuatro * 0.85;
    var cinco = precinco.toFixed(0);
    console.log(cinco + " resultado");

    clcr_value = cinco;

    var save_clcr_value1 = clcr_value;
    var save_clcr_value2 = JSON.stringify(save_clcr_value1);
    localStorage.setItem("stored_clcr", save_clcr_value2);
    console.log("saved");

    window.location.href = "atb.html";

});

document.getElementById("about").addEventListener("click", function () {

    window.location.href = "about.html";

});

//MDRD hombre
document.getElementById("mdrd_sex_h").addEventListener("click", function () {

    var edad = document.getElementById("mdrd_edad").value;
    var creatinina = document.getElementById("mdrd_cr").value;
    var creatE = Math.pow(creatinina, -1.154);
    var edadE = Math.pow(edad, -0.203);

    var resultado = 175 * +creatE * +edadE;
    var resfixed = resultado.toFixed(0);

    clcr_value = resfixed;

    var save_clcr_value1 = clcr_value;
    var save_clcr_value2 = JSON.stringify(save_clcr_value1);
    localStorage.setItem("stored_clcr", save_clcr_value2);
    console.log("saved");

    window.location.href = "atb.html";
 

    

});

//MDRD mujer
document.getElementById("mdrd_sex_m").addEventListener("click", function () {

    var edad = document.getElementById("mdrd_edad").value;
    var creatinina = document.getElementById("mdrd_cr").value;
    var creatE = Math.pow(creatinina, -1.154);
    var edadE = Math.pow(edad, -0.203);

    var resultado = 175 * +creatE * +edadE * 0.742;
    var resfixed = resultado.toFixed(0);

    clcr_value = resfixed;

    var save_clcr_value1 = clcr_value;
    var save_clcr_value2 = JSON.stringify(save_clcr_value1);
    localStorage.setItem("stored_clcr", save_clcr_value2);
    console.log("saved");

    window.location.href = "atb.html";

    
});

//CKD hombre
document.getElementById("ckd_sex_h").addEventListener("click", function () {

    var creatinina = document.getElementById("ckd_cr").value;
    var edad = document.getElementById("ckd_edad").value;

    if (creatinina > 0.9) {

        var cr1 = +creatinina / 0.9;
       
        var cr2 = Math.pow(+cr1, -1.209);

        var edad1 = Math.pow(0.993, +edad);

        var preresultado = 141 * +cr2 * edad1;
        var resultado = preresultado.toFixed(0);

        console.log(preresultado);
        console.log(resultado);

    } else if (creatinina <= 0.9) {

        var cr1 = +creatinina / 0.9;

        var cr2 = Math.pow(+cr1, -0.411);

        var edad1 = Math.pow(0.993, +edad);

        var preresultado = 141 * +cr2 * edad1;
        var resultado = preresultado.toFixed(0);

        console.log(preresultado);
        console.log(resultado);

    }
  

    clcr_value = resultado;

    var save_clcr_value1 = clcr_value;
    var save_clcr_value2 = JSON.stringify(save_clcr_value1);
    localStorage.setItem("stored_clcr", save_clcr_value2);
    console.log("saved");

    window.location.href = "atb.html";


});

//CKD mujer
document.getElementById("ckd_sex_m").addEventListener("click", function () {

    var creatinina = document.getElementById("ckd_cr").value;
    var edad = document.getElementById("ckd_edad").value;

    if (creatinina > 0.7) {

        var cr1 = +creatinina / 0.7;

        var cr2 = Math.pow(+cr1, -1.209);

        var edad1 = Math.pow(0.993, +edad);

        var preresultado = 144 * +cr2 * edad1;
        var resultado = preresultado.toFixed(0);

        console.log(preresultado);
        console.log(resultado);

    } else if (creatinina <= 0.7) {

        var cr1 = +creatinina / 0.7;

        var cr2 = Math.pow(+cr1, -0.329);

        var edad1 = Math.pow(0.993, +edad);

        var preresultado = 144 * +cr2 * edad1;
        var resultado = preresultado.toFixed(0);

        console.log(preresultado);
        console.log(resultado);

    }


    clcr_value = resultado;

    var save_clcr_value1 = clcr_value;
    var save_clcr_value2 = JSON.stringify(save_clcr_value1);
    localStorage.setItem("stored_clcr", save_clcr_value2);
    console.log("saved");

    window.location.href = "atb.html";


});